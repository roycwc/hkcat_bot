const config = require('./config.json');
const request = require('request');
const TelegramBot = require('node-telegram-bot-api');
const md5 = require('md5');

function getCat(){
	return new Promise((resolve)=>{
		request('http://thecatapi.com/api/images/get',(err,response, body)=>{
			resolve(response.request.uri.href)
		})
	});
}
function getDog(){
	let randomText = md5(Math.random());
	return Promise.resolve('http://loremflickr.com/320/240/puppy?'+randomText);
}

function ini(){
	let bot = new TelegramBot(config.tg, { polling: true });
	bot.onText(/^cat$/i, function (msg) {
		let chatId = msg.chat.id;
		getCat().then((url)=>{
			bot.sendPhoto(chatId, url);
		})
	})
	bot.onText(/^dog$/i, function (msg) {
		let chatId = msg.chat.id;
		getDog().then((url)=>{
			bot.sendPhoto(chatId, url);
		})
	})
}

ini()
